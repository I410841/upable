function loadMore() {
    var check = document.getElementById("check");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");

    if (check.style.display === "none") {
        check.style.display = "inline";
        btnText.innerHTML = "Meer informatie ᐯ";
        moreText.style.display = "none";
    } else {
        check.style.display = "none";
        btnText.innerHTML = "Minder informatie ᐱ";
        moreText.style.display = "inline";
    }
}